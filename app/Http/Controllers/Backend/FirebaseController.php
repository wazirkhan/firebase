<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Kreait\Firebase\Database;

class FirebaseController extends Controller
{
    public function __construct(Database $database)
    {

        $this->database = $database;
        // $this->tablename="contacts";
    }

    public function getData()
    {

        $data = $this->database->getReference()->getValue();
        // dd($data);
        return view('backend.get_data', compact('data'));
    }
}
