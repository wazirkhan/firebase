<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWrittenTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('written_tests', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->text('about')->nullable();
            $table->text('instruction')->nullable();
            $table->text('bar_chart')->nullable();
            $table->text('line_graph')->nullable();
            $table->text('pie_chart')->nullable();
            $table->text('multiple_graphs')->nullable();
            $table->text('process_diagram')->nullable();
            $table->text('table_chart')->nullable();
            $table->text('maps')->nullable();
            $table->text('opinion_essay')->nullable();
            $table->text('discussion_essay')->nullable();
            $table->text('process_solution_essay')->nullable();
            $table->text('advantages_disadvantages_essay')->nullable();
            $table->text('double_question_essay')->nullable();
            $table->string('type')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('written_tests');
    }
}
