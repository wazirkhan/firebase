<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'firebase' => [
        'apiKey' => "AIzaSyB0WoXxATPxBty-6rzGnhYl1Iw3kbcCG-8",
        'authDomain' => "laravelfirebase-d63a4.firebaseapp.com",
        'databaseURL'=> "https://laravelfirebase-d63a4-default-rtdb.firebaseio.com",
        'storageBucket' => "laravelfirebase-d63a4.appspot.com",
        'messagingSenderId' => "764039562225",
        'appId' =>  "1:764039562225:web:30f4f777de5716fa997a50",
    ],

];
