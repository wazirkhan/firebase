<div class="footer">
    <div class="copyright">
        <p>Copyright © Designed &amp; Developed by <a href="#" target="_blank">Lazzy Egg</a> @php echo date('Y') @endphp</p>
    </div>
</div>
