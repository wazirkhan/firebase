<div class="deznav">
    <div class="deznav-scroll">

        <ul class="metismenu" id="menu">
        <li>
            <a href="{{route('admin.dashboard')}}" class="ai-icon" aria-expanded="false">
                <i class="flaticon-381-networking"></i>
                <span class="nav-text">Dashoard</span>
            </a>
        </li>
        <li>
            <a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
            <i class="flaticon-381-controls-3"></i>
            <span class="nav-text">Get Data</span>
            </a>
            <ul aria-expanded="false">
                <li><a href="{{route('admin.firebase_data')}}">firebase</a></li>
            </ul>
        </li>

        </ul>

    </div>
</div>
