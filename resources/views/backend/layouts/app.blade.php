
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" defer></script>


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Favicon icon -->

    @include('backend.partials.style')
    @stack('style')
</head>
<body>


    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>

    <div id="main-wrapper">

        @include('backend/includes/header')

        @include('backend/includes/navbar')


        @include('backend/includes/sidebar')

        @yield('content')

        @include('backend/includes/footer')

    </div>

    @include('backend.partials.scripts')
    @stack('scripts')

</body>
</html>
