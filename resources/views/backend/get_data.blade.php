@extends('backend.layouts.app')
<title>Admin | Firebase Data </title>

@section('title')
Dashboard
@endsection

@push('style')

@endpush

@section('content')
<div class="content-body">

	<div class="container-fluid">
		<!-- Add Order -->
		<div class="page-titles">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
				<li class="breadcrumb-item active"><a href="#">Tests</a></li>
			</ol>
		</div>
		@if(session()->has('success_massage'))
		<div class="alert alert-success alert-dismissible fade show">
			<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
			<strong>Success!</strong> {{session()->get('success_massage')}}
			<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
			</button>
		</div>
		@endif

        @if(session()->has('delete'))
		<div class="alert alert-danger alert-dismissible fade show">
			<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
			<strong>Success!</strong> {{session()->get('delete')}}
			<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
			</button>
		</div>
		@endif



		<div class="row"><div class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">getting data</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered verticle-middle table-responsive-sm">
							<thead>
								<tr>
									<th scope="col">Message</th>
                                    <th scope="col">Number</th>
								</tr>
							</thead>
							<tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item['message']}}</td>
                                        <td>{{$item['number']}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection


@push('scripts')

@endpush
